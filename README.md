# Radicale

Docker image for [Radicale](https://radicale.org/).

## Usage

Setup [Traefik Proxy](https://gitlab.com/wp-id/docker/traefik-proxy), then use the example compose file in this repository:

```sh
git clone https://gitlab.com/kucrut/radicale.git
cd radicale/compose
cp config/config.example config/config
# Edit config/config as needed.
cp .env.example .env
# Edit .env as needed.
ln -s docker-compose.override.traefik-proxy.yml docker-compose.override.yml
docker compose up -d
```
