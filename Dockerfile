FROM alpine:latest

LABEL Maintainer="Dzikri Aziz" Description="Radicale: Simple calendar (CalDAV) and contact (CardDAV) server"

# Version of Radicale (e.g. 3.0.x)
ARG VERSION=3.1.8

# Install dependencies
RUN apk add --no-cache --update \
      python3 \
      py3-pip \
      py3-wheel \
      ca-certificates \
      openssl && \
    apk add --no-cache --update --virtual .build-deps \
      python3-dev \
      build-base \
      libffi-dev && \
    # Install Radicale
    wget --quiet https://github.com/Kozea/Radicale/archive/refs/tags/v${VERSION}.tar.gz --output-document=radicale.tar.gz && \
    tar xzf radicale.tar.gz && \
    pip3 install ./Radicale-${VERSION} && \
    rm -r radicale.tar.gz Radicale-${VERSION} && \
    # Remove build dependencies
    apk del .build-deps
# Persistent storage for data (Mount it somewhere on the host!)
VOLUME /var/lib/radicale
# Configuration data (Put the "config" file here!)
VOLUME /etc/radicale
# TCP port of Radicale (Publish it on a host interface!)
EXPOSE 5232
# Run Radicale (Configure it here or provide a "config" file!)
CMD ["radicale", "--hosts", "0.0.0.0:5232"]
